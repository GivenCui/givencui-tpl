# js算法模板
## 配置vscode debug参考
- [vscode官网](https://code.visualstudio.com/docs/editor/debugging)
- [踩坑：vscode中jest的调试配置](https://blog.csdn.net/qq_17371033/article/details/79503136)


## 技术选型
- babel
- jest
- Majestic

> 优点: jest运行单元测试, majestic提供ui可视化, vscode支持debug

## 使用
拷贝算法练习项目模板, 并改名为 leetcodePractice (举例)

安装
```sh
cd ./leetcodePractice

# 初始化
npm init -y

# 安装依赖
yarn add babel-jest jest @babel/preset-env majestic -D

```
配置文件
```json
// 配置文件 .babelrc
{
  "presets": ["@babel/preset-env"]
}


// package.json
{
  "scripts": {
    "test": "jest ./test",
    "ui": "majestic --debug"
  },
  "devDependencies": {
    "@babel/preset-env": "^7.11.5",
    "babel-jest": "^26.3.0",
    "jest": "^26.4.2",
    "majestic": "^1.7.0"
  }
}

```
## vscode中debug模式运行脚本
选择图中的 `调试脚本`

![步骤一](http://img.givencui.com/2020-09-03-070759.jpg)
